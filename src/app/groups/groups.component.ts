import {Component, OnInit} from '@angular/core';
import {FacultyService} from "../services/faculty.service";
import {Faculty} from "../interfaces/faculty";
import {GroupService} from "../services/group.service";
import {Group} from "../interfaces/group";


@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {
  faculties: Faculty[] = [];
  currentFacultyID: number = 1;
  groups: Group[] = [];


  constructor(private facultyService: FacultyService, private groupService: GroupService) {
  }


  getFaculties(): void {
    this.facultyService.getFaculties().then(faculties => this.faculties = faculties)
  }

  ngOnInit() {
    this.getFaculties();
    this.getGroups();
    console.log(this.currentFacultyID);
  }

  onChangeFaculty(id: number) {
    this.currentFacultyID = id;
  }

  getGroups() {
    this.groupService.getGroups(this.currentFacultyID)
      .then( groups => this.groups = groups)
  }
}

export class Student {
  id: number;
  first_name: string;
  last_name: string;
  "image": any;
  "groups_id": number;
  name: string;
  group_name: string;
}

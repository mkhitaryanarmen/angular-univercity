import {NgModule} from '@angular/core';
import {Routes, RouterModule} from "@angular/router";

import {FacultyComponent} from "./faculty/faculty.component";
import {StudentsComponent} from "./students/students.component";
import {AddStudentComponent} from "./students/add-student/add-student.component";
import {GroupsComponent} from "./groups/groups.component";


// const addStudentRoutes: Routes = [
//   {path: 'add-students', component: AddStudentComponent}
// ];

const routes: Routes = [
  {path: '', redirectTo: '/students', pathMatch: 'full'},
  {path: 'faculty', component: FacultyComponent},
  {path: 'group', component: GroupsComponent},
  {path: 'students', component: StudentsComponent},
  {path: 'addStudents', component: AddStudentComponent}
];







@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}

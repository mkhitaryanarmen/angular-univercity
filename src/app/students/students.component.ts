import {Component, OnInit} from '@angular/core';
import {StudentService} from "../services/student.service";
import { Router } from "@angular/router";


@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  students = [];

  constructor(private studentService: StudentService, private router: Router) {
  }

  getStudents(): void {
    this.studentService.getStudents().then(students => this.students = students)
  }

  ngOnInit() {
    this.getStudents();
  }


  goToAddStudent(): void {
    this.router.navigate(['/addStudents']);
  }

}

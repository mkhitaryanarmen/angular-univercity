/* Modules */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from "@angular/http";
/* Routes Modules */
import { AppRoutingModule } from "./app-routing.module";
/* Components */
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FacultyComponent } from './faculty/faculty.component';
import { GroupsComponent } from './groups/groups.component';
import { StudentsComponent } from './students/students.component';
import { AddStudentComponent } from './students/add-student/add-student.component';
/* Services */
import {FacultyService} from "./services/faculty.service";
import {StudentService} from "./services/student.service";
import {GroupService} from "./services/group.service";



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FacultyComponent,
    StudentsComponent,
    AddStudentComponent,
    GroupsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [FacultyService, StudentService, GroupService],
  bootstrap: [AppComponent]
})
export class AppModule { }

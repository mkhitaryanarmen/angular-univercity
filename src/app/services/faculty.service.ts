import {Injectable} from "@angular/core";
import {Http, Headers} from "@angular/http";
import 'rxjs/add/operator/toPromise';
import {Faculty} from "../interfaces/faculty";


@Injectable()
export class FacultyService {

  constructor(private http: Http) {
  }

  private headers = new Headers({'Content-Type': 'application/json'});

  getFaculties(): Promise<Faculty[]> {
    return this.http.get('http://127.0.0.1:8000/api/faculties')
      .toPromise()
      .then(response => response.json().faculties as Faculty[])
      .catch(this.handleError);
  }

  createFaculty(name: string): Promise<Faculty> {
    return this.http.post(
      'http://127.0.0.1:8000/api/faculty', {'name': name}, {headers: this.headers})
      .toPromise()
      .then(res => res.json().faculty as Faculty)
      .catch(this.handleError);
  }


  update(id: number, name: string): Promise<Faculty> {
      return this.http.put('http://127.0.0.1:8000/api/faculty/' + id, JSON.stringify({name: name}), {headers: this.headers})
        .toPromise()
        .then((response) => response.json().faculty)
        .catch(this.handleError);
  }

  delete(id: number){
    return this.http.delete('http://127.0.0.1:8000/api/faculty/' + id, {headers: this.headers})
      .toPromise()
      .then(res => res.json())
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

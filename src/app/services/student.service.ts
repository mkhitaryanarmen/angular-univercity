import {Inject, Injectable} from "@angular/core";
import {Http, Headers} from "@angular/http";
import 'rxjs/add/operator/toPromise';
import {Student} from "../interfaces/student";

Injectable()
export class StudentService {

  constructor(@Inject(Http) private http: Http) {}

  getStudents(): Promise<Student[]> {
    return this.http.get('http://127.0.0.1:8000/api/students')
      .toPromise()
      .then((response) => {return response.json().students as Student[]})
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}


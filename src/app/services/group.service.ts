import {Http} from "@angular/http";
import {Injectable} from "@angular/core";
import 'rxjs/add/operator/toPromise';
import {Group} from "../interfaces/group";


@Injectable()
export class GroupService {
  constructor(private http: Http) {
  }

  getGroups(id: number): Promise<Group[]> {
    return this.http.get('http://127.0.0.1:8000/api/groups/' + id)
      .toPromise()
      .then(response => response.json() as Group[])
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}

import {Component, OnInit} from '@angular/core';
import {Faculty} from "../interfaces/faculty";
import {FacultyService} from "../services/faculty.service";



@Component({
  selector: 'app-faculty',
  templateUrl: './faculty.component.html',
  styleUrls: ['./faculty.component.css']
})
export class FacultyComponent implements OnInit {
  faculties: Faculty[];
  editing = '';
  editValue = '';


  constructor(private facultyService: FacultyService) {
  }

  getFaculties(): void {
    this.facultyService.getFaculties().then(faculties => this.faculties = faculties)
  }

  onAddFaculty(addFaculty: string): void {
    addFaculty = addFaculty.trim();
    if (!addFaculty) {return;}
    this.facultyService.createFaculty(addFaculty)
      .then(addFaculty => {
        this.faculties.push(addFaculty);
      });
  }


  onEdit(name: string): void {
    this.editValue = name;
    this.editing = name;
  }

  onExit() {
    this.editValue = '';
  }

  onSave(faculty: Faculty, name: string,): void {
    this.facultyService.update(faculty.id, name)
      .then(faculty =>  faculty);
      faculty.name = name;

    this.editValue = '';
  }


  ngOnInit() {
    this.getFaculties();
  }


  onDelete(faculty: Faculty) {
    this.facultyService.delete(faculty.id)
      .then((res) => console.log(res));

    const position = this.faculties.findIndex(
      (facultyEl: Faculty) => {
        return facultyEl.id === faculty.id;
      });
    this.faculties.splice(position,1);
  }

}
